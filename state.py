from typing import Dict, List


class State:
    def __init__(self) -> None:
        pass

    def variables(self) -> List[str]:
        return list(self.__dict__.keys())

    def dict(self) -> Dict:
        return self.__dict__


class StateRoomTemperatureHumidity(State):
    """Room temperature and humidity state.

    Attributes:
        room_temperature: Temperature in degrees Celsius.
        room_humidity: Humidity in percent.
    """

    def __init__(self) -> None:
        """Initialises a temperature and humidity state."""
        super(StateRoomTemperatureHumidity, self).__init__()

        self.room_temperature = None  # type: float
        self.room_humidity = None  # type: float


class StateCpuTemperature(State):
    """CPU temperature state

    Attributes:
        cpu_temperature: Temperature per CPU core.
        cpu_temperature_high: High threshold temperature per CPU core.
        cpu_temperature_critical: Critical threshold temperature per CPU core.
    """

    def __init__(self) -> None:
        """Initialises a CPU temperature state."""
        super(StateCpuTemperature, self).__init__()

        self.cpu_temperature = None  # type: List[float]
        self.cpu_temperature_high = None  # type: List[float]
        self.cpu_temperature_critical = None  # type: List[float]


class StateCpuLoad(State):
    """CPU loading state.

    Attributes:
        cpu_load: Percentage CPU load per core.
    """

    def __init__(self) -> None:
        """Initialises a CPU loading state."""
        super(StateCpuLoad, self).__init__()

        self.cpu_load = None  # type: List[float]


class StateMemoryLoad(State):
    """Memory loading state.

    Attributes:
        memory_total: Total memory in MiB.
        memory_used: Used memory in MiB.
        swap_total: Total swap in MiB.
        swap_used: Used swap in MiB.
    """

    def __init__(self) -> None:
        """Initialises a memory loading state."""
        super(StateMemoryLoad, self).__init__()

        self.memory_total = None  # type: int
        self.memory_used = None  # type: int
        self.swap_total = None  # type: int
        self.swap_used = None  # type: int


class StateWeather(State):
    """Weather state.

    Attributes:
        outside_humidity: Outside humidity in percent.
        outside_temperature: Outside temperature in degrees Celsius.
    """

    def __init__(self) -> None:
        """Initialises a weather state."""
        super(StateWeather, self).__init__()

        self.outside_humidity = None
        self.outside_temperature = None
