#!/opt/anaconda3/bin/python

import argparse
import datetime
import itertools
import re
import sys
from typing import List

import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import numpy as np


class LogEntryNode:
    def __init__(self, name: str, num_cores: int, cpu_load: float,
                 mem_used: float, swap_used: float, temperature: float) -> None:
        self.name = name  # type: str
        self.num_cores = num_cores  # type: int
        self.cpu_load = cpu_load  # type: float
        self.mem_used = mem_used  # type: float
        self.swap_used = swap_used  # type: float
        self.temperature = temperature  # type: float


class LogEntryClimate:
    def __init__(self, room_temperature: float, room_humidity: float,
                 outside_temperature: float, outside_humidity: float) -> None:
        self.room_temperature = room_temperature
        self.room_humidity = room_humidity
        self.outside_temperature = outside_temperature
        self.outside_humidity = outside_humidity


class LogEntry:
    def __init__(self, time: datetime.datetime, climate: LogEntryClimate,
                 nodes: List[LogEntryNode]) -> None:
        self.time = time  # type: datetime.datetime
        self.climate = climate  # type: LogEntryClimate
        self.nodes = nodes  # type: List[LogEntryNode]


class Log:
    def __init__(self, entries: List[LogEntry]) -> None:
        self.entries = entries

    @property
    def num(self) -> int:
        return len(self.entries)

    @property
    def time(self) -> np.ndarray:
        return np.array([entry.time for entry in self.entries])

    @property
    def room_temperature(self) -> np.ndarray:

        room_temperature = np.zeros(self.num)

        count = itertools.count()
        for entry in self.entries:
            if entry.climate.room_temperature is None:
                room_temperature[next(count)] = np.nan
            else:
                room_temperature[next(count)] = entry.climate.room_temperature

        return room_temperature

    @property
    def room_humidity(self) -> np.ndarray:

        room_humidity = np.zeros(self.num)

        count = itertools.count()
        for entry in self.entries:
            if entry.climate.room_temperature is None:
                room_humidity[next(count)] = np.nan
            else:
                room_humidity[next(count)] = entry.climate.room_humidity

        return room_humidity

    @property
    def outside_humidity(self) -> np.ndarray:

        outside_humidity = np.zeros(self.num)

        count = itertools.count()
        for entry in self.entries:
            if entry.climate.outside_temperature is None:
                outside_humidity[next(count)] = np.nan
            else:
                outside_humidity[next(count)] = entry.climate.outside_humidity

        return outside_humidity

    @property
    def outside_temperature(self) -> np.ndarray:

        outside_temperature = np.zeros(self.num)

        count = itertools.count()
        for entry in self.entries:
            if entry.climate.outside_temperature is None:
                outside_temperature[next(count)] = np.nan
            else:
                outside_temperature[next(count)] = \
                    entry.climate.outside_temperature

        return outside_temperature

    @property
    def total_cpu_load(self) -> np.ndarray:

        count = itertools.count()
        cpu_load = np.zeros(self.num)
        for entry in self.entries:
            load_cur = 0
            num_cores_cur = 0
            for node in entry.nodes:
                load_cur += node.cpu_load * node.num_cores
                num_cores_cur += node.num_cores
            if len(entry.nodes) > 0:
                cpu_load[next(count)] = load_cur / num_cores_cur
            else:
                cpu_load[next(count)] = np.NAN

        return cpu_load


class LogParser:
    def __init__(self, file: str) -> None:
        self.file = file  # type: str

    def parse(self) -> Log:
        """Parses a ClusterSensors log file."""

        with open(self.file) as file:
            raw = file.readlines()

        entries = []
        for line in raw:
            line = line.strip()

            # Get the time and room temperature and humidity
            match = re.match(r"\[(?P<date>\d{4}-\d{2}-\d{2} "
                             r"\d{2}:\d{2}:\d{2}\.\d{6})\]: "
                             r"(?P<rtemp>[^;]+); (?P<rhumid>[^;]+); "
                             r"(?P<otemp>[^;]+); (?P<ohumid>[^;]+)"
                             r"(?P<nodes>.*)", line)
            time = datetime.datetime.strptime(match.group("date"),
                                              "%Y-%m-%d %H:%M:%S.%f")
            if match.group("rtemp") == "N/A":
                rtemp = None
                rhumid = None
            else:
                rtemp = float(match.group("rtemp"))
                rhumid = float(match.group("rhumid"))
            if match.group("otemp") == "N/A":
                otemp = None
                ohumid = None
            else:
                otemp = float(match.group("otemp"))
                ohumid = float(match.group("ohumid"))

            entry_room = LogEntryClimate(rtemp, rhumid, otemp, ohumid)

            match_nodes = re.findall(r"; (?P<name>[^:]+): "
                                     r"(?P<num_cores>\d+), "
                                     r"(?P<cpu_load>[\d.]+), "
                                     r"(?P<mem_used>[\d.]+), "
                                     r"(?P<swap_used>[\d.]+), "
                                     r"(?P<temp>[\d.]+)",
                                     match.group("nodes"))
            entries_node = []
            for match in match_nodes:
                entries_node.append(LogEntryNode(match[0],
                                                 int(match[1]),
                                                 float(match[2]),
                                                 float(match[3]),
                                                 float(match[4]),
                                                 float(match[5])))

            entries.append(LogEntry(time, entry_room, entries_node))

        return Log(entries)


def main():
    arg_parser = argparse.ArgumentParser(prog="ClusterSensors log parser")
    arg_parser.add_argument("-f", "--file", default="/var/log/clustersensors")
    arg_parser.add_argument("-r", "--range", action="store", default="day",
                            choices=["month", "week", "day"])

    args = arg_parser.parse_args(sys.argv[1:])

    parser = LogParser(args.file)
    log = parser.parse()

    time_end = log.time[-1]
    if args.range == "month":
        time_start = time_end - datetime.timedelta(days=31)
    elif args.range == "week":
        time_start = time_end - datetime.timedelta(days=7)
    elif args.range == "day":
        time_start = time_end - datetime.timedelta(days=1)
    else:
        raise ValueError("Invalid range.")

    is_valid = log.time >= time_start
    time = log.time[is_valid]
    total_cpu_load = log.total_cpu_load[is_valid]
    room_temperature = log.room_temperature[is_valid]
    outside_temperature = log.outside_temperature[is_valid]

    h_fig = plt.figure()

    ax = plt.subplot(3, 1, 1)
    plt.plot(time, total_cpu_load, ".-k")
    plt.ylabel("Total CPU load [%]")

    plt.subplot(3, 1, 2, sharex=ax)
    plt.plot(time, room_temperature, ".-k")
    plt.ylabel("Room temperature [°C]")

    ax_bottom = plt.subplot(3, 1, 3, sharex=ax)
    plt.plot(time, outside_temperature, ".-k")
    plt.xlabel("Date")
    plt.ylabel("Outside temperature [°C]")

    ax_bottom.xaxis.set_major_formatter(mdates.DateFormatter("%Y-%m-%d %H:%M"))
    h_fig.autofmt_xdate()

    plt.show()


if __name__ == "__main__":
    main()
