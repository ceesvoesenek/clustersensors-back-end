from sensor import SensorMemoryLoad


def main() -> None:
    sensor = SensorMemoryLoad()

    print(sensor.get().memory_total)


if __name__ == "__main__":
    main()