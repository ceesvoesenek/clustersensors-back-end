import time

from sensor import SensorWeather


def main() -> None:
    sensor = SensorWeather(update_interval=5)

    while True:
        state = sensor.get()
        print(state.outside_humidity, state.outside_temperature)

        time.sleep(1)


if __name__ == "__main__":
    main()
