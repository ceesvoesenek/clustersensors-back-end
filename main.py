#!/opt/anaconda3/bin/python

import argparse
import sys

from client import ClusterSensorsClient
from server import ClusterSensorsServer


def main() -> None:
    parser = argparse.ArgumentParser(prog="ClusterSensors")
    subparsers = parser.add_subparsers(help="Type of ClusterSensors session",
                                       dest="session_type")
    subparsers.required = True

    parser_server = subparsers.add_parser("server", help="Run ClusterSensors "
                                                         "as server")
    parser_server.add_argument("-j", "--json", action="store", required=True,
                               nargs=2, help="Path to the JSON file to output "
                                             "to and the output interval "
                                             "in seconds")
    parser_server.add_argument("-l", "--log", action="store", required=True,
                               nargs=2, help="Path to the log file to output "
                                             "to and the output interval in "
                                             "minutes")
    parser_server.add_argument("-p", "--port", action="store", default=5000,
                               type=int, help="Port to listen on")

    parser_client = subparsers.add_parser("client", help="Run ClusterSensors "
                                                         "as client")
    parser_client.add_argument("-s", "--server", action="store", required=True,
                               help="The IP of the server to connect to")
    parser_client.add_argument("-t", "--type", action="store", default="node",
                               choices=["node", "front"],
                               help="The type of computer the client is "
                                    "running on.")
    parser_client.add_argument("-p", "--port", action="store", default=5000,
                               type=int, help="Port to connect to on the "
                                              "server")

    args = parser.parse_args(sys.argv[1:])
    if args.session_type == "server":
        server = ClusterSensorsServer(args.json[0], float(args.json[1]),
                                      args.log[0], float(args.log[1])*60,
                                      args.port)
        server.run()
    else:
        client = ClusterSensorsClient(args.server, args.type, args.port)
        client.run()


if __name__ == "__main__":
    main()
