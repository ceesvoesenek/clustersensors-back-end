from typing import Any, Dict, List

from sensor import Sensor


class SensorArray:
    """A collection of sensors."""

    def __init__(self) -> None:
        self.sensors = []  # type: List[Sensor]

    def add(self, sensor: Sensor) -> None:
        """Adds a sensor to the collection.

        Args:
            sensor: The sensor to add.
        """
        self.sensors.append(sensor)

    def dict(self) -> Dict[str, Any]:
        """Returns a dictionary with the collected values of the sensors.

        Returns:
            A dictionary with sensor values.
        """

        # Everything is combined into a single dictionary here.
        val = dict()
        for sensor in self.sensors:
            val = {**val, **sensor.get().dict()}

        return val
