import pickle
import socket
import time
from typing import Union

from sensor import SensorCpuLoad, SensorCpuTemperature, SensorMemoryLoad, \
    SensorRoomTemperatureHumidity, SensorWeather
from sensorarray import SensorArray


class ClusterSensorsClient:
    """A client for cluster sensors.

    Attributes:
        server: The host IP to connect to.
        port: The port to connect to.
        client_type: The type of computer this is running on, "node" or "front".
    """

    def __init__(self, server: str, client_type: str, port: int = 5000) -> None:
        """Initialises a cluster sensors client.

        Args:
            server: The host IP to connect to.
            client_type: The type of computer this is running on,
                "node" or "front".
            port: The port to connect to.
        """
        self.server = server  # type: str
        self.port = port  # type: int
        self.client_type = client_type  # type: str
        self._array = SensorArray()  # type: SensorArray
        self._conn = None  # type: socket.socket

    def run(self) -> None:
        """Runs a client that sends sensor data to the server."""
        self._setup_sensors()
        self._connect()

        while True:
            data = self._array.dict()
            data["client_type"] = self.client_type

            success = self._send(pickle.dumps(data))
            # If we could not send data, attempt to find a server.
            if not success:
                print("Connection dropped")
                self._connect()

            time.sleep(1)

    def _connect(self) -> None:
        """Keeps running until it found a server."""

        self._conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        print("Connecting to server...", end="")
        connected = False
        while not connected:
            try:
                self._conn.connect((self.server, self.port))
                print("connected")
                connected = True
            except (ConnectionError, OSError):
                time.sleep(1)

        # Send the host name as a handshake
        self._send(socket.gethostname())

    def _send(self, message: Union[str, bytes]) -> bool:
        """Sends a message over the associated socket."""
        if type(message) is str:
            message = message.encode()

        # Encode the length into a byte string, ending in a colon
        length = len(message)
        header = str(length) + ":"
        header = header.encode()

        success = True
        try:
            self._conn.sendall(header + message)
        except (BrokenPipeError, ConnectionResetError):
            success = False

        return success

    def _setup_sensors(self) -> None:
        """Sets up the sensor array depending on the type of computer."""

        if self.client_type == "node":
            self._array.add(SensorCpuTemperature())
            self._array.add(SensorCpuLoad())
            self._array.add(SensorMemoryLoad())
        elif self.client_type == "front":
            self._array.add(SensorRoomTemperatureHumidity("/dev/ttyACM1", 9600))
            self._array.add(SensorWeather())

