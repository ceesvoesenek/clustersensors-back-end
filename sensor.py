from abc import ABCMeta
import serial
import subprocess
import re
import threading
import time
import urllib.request
import xml.etree.ElementTree

from state import State, StateCpuLoad, StateCpuTemperature, StateMemoryLoad, \
    StateRoomTemperatureHumidity, StateWeather


class Sensor(metaclass=ABCMeta):
    def __init__(self) -> None:
        pass

    def get(self) -> State:
        pass


class SensorRoomTemperatureHumidity(Sensor):
    """Sensor for room temperature and humidity."""

    def __init__(self, terminal: str, baud_rate: float=9600):
        """Initialises a temperature and humidity sensor.

        Args:
            terminal: Path to the terminal that the sensor is logging to.
            baud_rate: Baud rate of the serial connection.
        """
        super(SensorRoomTemperatureHumidity, self).__init__()

        self.terminal = \
            serial.Serial(terminal, baud_rate)  # type: serial.Serial
        self.state = \
            StateRoomTemperatureHumidity()  # type: StateRoomTemperatureHumidity

        # Start a thread that continously updates the temperature and
        # humidity of this sensor
        self._lock_state = threading.Lock()
        self.thread_update = threading.Thread(target=self._update, daemon=True)
        self.thread_update.start()

    def get(self) -> StateRoomTemperatureHumidity:
        """Gets the room temperature and humidity."""
        with self._lock_state:
            return self.state

    def _update(self) -> None:
        """Continously updates the room temperature and humidity.

        This reads a line from the serial connection as soon as it is
        available, and then updates the sensor's state. Note that the
        current state is always accessible as this function is run in a
        separate thread.
        """

        pat = r"^Humidity:\s*(?P<humidity>[.\d]+)\s*%\s*" \
              r"Temperature:\s*(?P<temperature>[.\d]+)\s*\*C\s*$"
        while True:
            raw = self.terminal.readline().decode("utf8")
            match = re.match(pat, raw)
            # Malformed output may occur, especially when starting the
            # stream, so if the pattern does not match, ignore it altogether
            if match is not None:
                with self._lock_state:
                    self.state.room_temperature = \
                        float(match.group("temperature"))
                    self.state.room_humidity = float(match.group("humidity"))


class SensorCpuTemperature(Sensor):
    """Sensor for CPU temperatures."""

    def __init__(self):
        """Initialises a CPU temperature sensor."""
        super(SensorCpuTemperature, self).__init__()

        self.state = StateCpuTemperature()  # type: StateCpuTemperature

    def get(self) -> StateCpuTemperature:
        """Gets the CPU temperatures."""
        self._update()
        return self.state

    def _update(self) -> None:
        """Updates the current CPU temperatures."""

        raw = subprocess.check_output("sensors").decode("utf8").split("\n")
        self.state.cpu_temperature = []
        self.state.cpu_temperature_high = []
        self.state.cpu_temperature_critical = []
        for line in raw:
            match = re.match(r"Core \d+:\s*(?P<cur>[-+\d.]+)°C"
                             r"\s*\(high = (?P<high>[-+\d.]+)°C"
                             r"\s*,\s*crit = (?P<crit>[-+\d.]+)°C\)", line)
            if match is not None:
                cur = float(match.group("cur"))
                high = float(match.group("high"))
                crit = float(match.group("crit"))

                self.state.cpu_temperature.append(cur)
                self.state.cpu_temperature_high.append(high)
                self.state.cpu_temperature_critical.append(crit)


class SensorCpuLoad(Sensor):
    """Sensor for CPU and memory loading."""

    def __init__(self):
        """Initialises a CPU and memory loading sensor."""
        super(SensorCpuLoad, self).__init__()

        self.state = StateCpuLoad()
        self.total_prev = []
        self.idle_prev = []

    def get(self) -> StateCpuLoad:
        self._update()
        return self.state

    def _update(self) -> None:
        """Updates the CPU load."""

        with open("/proc/stat", "r") as file:
            raw = file.readlines()

        self.state.cpu_load = []
        core = 0
        for line in raw[1:]:
            match = re.match(r"cpu\d+\s+"
                             r"(?P<user>\d+)\s+"
                             r"(?P<nice>\d+)\s+"
                             r"(?P<system>\d+)\s+"
                             r"(?P<idle>\d+)\s+"
                             r"(?P<iowait>\d+)\s+"
                             r"(?P<irq>\d+)\s+"
                             r"(?P<softirq>\d+)\s+", line)
            if match is not None:
                total = int(match.group("user")) + \
                        int(match.group("nice")) + \
                        int(match.group("system")) + \
                        int(match.group("idle")) + \
                        int(match.group("iowait")) + \
                        int(match.group("irq")) + \
                        int(match.group("softirq"))
                idle = int(match.group("idle"))

                if len(self.total_prev) < core+1:
                    total_cur = total
                    idle_cur = idle
                    self.total_prev.append(total)
                    self.idle_prev.append(idle)
                else:
                    total_cur = total - self.total_prev[core]
                    idle_cur = idle - self.idle_prev[core]
                    self.total_prev[core] = total
                    self.idle_prev[core] = idle

                self.state.cpu_load.append((total_cur-idle_cur)/total_cur*100.0)

                core += 1


class SensorMemoryLoad(Sensor):
    """Sensor for memory loading."""

    def __init__(self) -> None:
        """Initialises a memory loading sensor."""
        super(SensorMemoryLoad, self).__init__()

        self.state = StateMemoryLoad()

    def get(self) -> StateMemoryLoad:
        self._update()
        return self.state

    def _update(self) -> None:
        """Updates the memory load."""

        raw = subprocess.check_output(["free", "-m"])\
            .decode("utf8").split("\n")
        tokens_mem = raw[1].split()
        tokens_swap = raw[2].split()

        self.state.memory_total = int(tokens_mem[1])
        self.state.memory_used = int(tokens_mem[2])
        self.state.swap_total = int(tokens_swap[1])
        self.state.swap_used = int(tokens_swap[2])


class SensorWeather(Sensor):
    """Sensor for the Weather."""

    def __init__(self, update_interval=300) -> None:
        """Initialises a weather sensor.

        Args:
            update_interval: The interval between weather updates.
        """
        super(SensorWeather, self).__init__()

        self.update_interval = update_interval

        self.state = StateWeather()

        # Start a thread that continously updates the temperature and
        # humidity of this sensor
        self._lock_state = threading.Lock()
        self.thread_update = threading.Thread(target=self._update, daemon=True)
        self.thread_update.start()

    def get(self) -> StateWeather:
        with self._lock_state:
            return self.state

    def _update(self) -> None:
        while True:
            with urllib.request.urlopen("http://xml.buienradar.nl") as response:
                root = xml.etree.ElementTree.fromstring(response.read())
                for station in root.findall(".//weerstation[@id=\"6275\"]"):
                    with self._lock_state:
                        self.state.outside_humidity = \
                            float(station.findall("luchtvochtigheid")[0].text)
                        self.state.outside_temperature = \
                            float(station.findall("temperatuurGC")[0].text)
            time.sleep(self.update_interval)
