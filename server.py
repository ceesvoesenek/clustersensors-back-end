import datetime
import json
import pickle
import socket
import sys
import threading
import time
from typing import Any, Dict, Tuple


class ClientConnection:
    """A connection to a cluster sensors client.

    Attributes:
        conn: The socket to connect with.
        buffer_size: The size of the receive buffer.
        name: Hostname of the client.
    """

    def __init__(self, conn: socket.socket, buffer_size: int) -> None:
        self.conn = conn
        self.buffer_size = buffer_size
        self._buffer = b""
        self.name = ""

        # The first message upon creation of the connection is the host name,
        # the rest will be sensor data
        self.name = self.receive()[0].decode()

    def receive(self) -> Tuple[bytes, bool]:
        """Receives a complete message."""

        # First, read the header to check how long the message will be
        length = 0
        message = b""
        success = True
        while True:
            # Check whether we have reached the end of the header
            if b":" in self._buffer:
                idx = self._buffer.find(b":")
                length = int(self._buffer[:idx].decode())
                self._buffer = self._buffer[idx+1:]
                break

            # If we haven't, receive more data
            raw = self.conn.recv(self.buffer_size)
            if not raw:
                success = False
                break
            self._buffer += raw

        if success:
            while True:
                # Check whether we have received the complete message.
                if len(self._buffer) >= length:
                    message = self._buffer[:length]
                    self._buffer = self._buffer[length:]
                    break

                # If we haven't, receive more data
                raw = self.conn.recv(self.buffer_size)
                if not raw:
                    return b"", False
                self._buffer += raw

        return message, success


class ClusterSensorsServer:
    """A server for cluster sensors.

    Attributes:
        file_json: The JSON file to output to.
        interval_json: The interval in seconds between JSON-outputs.
        file_log: The log file to output to.
        interval_log: The interval in seconds between log outputs.
        port: The port to listen on.
        buffer_size: The data buffer size.
    """

    def __init__(self, file_json: str, interval_json: float,
                 file_log: str, interval_log: float,
                 port: int = 5000, buffer_size: int = 4096) -> None:
        """Initialises a cluster sensors server.

        Args:
            file_json: The JSON file to output to.
            interval_json: The interval in seconds between JSON outputs.
            file_log: The log file to output to.
            interval_log: The interval in seconds between log outputs.
            port: The port to listen on.
            buffer_size: The data buffer size.
        """
        self.file_json = file_json  # type: str
        self.interval_json = interval_json  # type: float
        self.file_log = file_log  # type: str
        self.interval_log = interval_log  # type: float
        self.port = port  # type: int
        self.buffer_size = buffer_size  # type: int

        self._data = dict()  # type: Dict[str, Dict[str, Any]]
        self._lock_data = threading.Lock()  # type: threading.Lock

    def run(self) -> None:
        """Runs a cluster sensors server."""
        print("Starting ClusterSensors server...")

        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            sock.bind(("", self.port))
        except socket.error:
            print("Cannot create server - bind to socket failed")
            sys.exit(1)

        print("Listening for connections...")
        sock.listen(10)

        # Start the JSON outputting thread and log outputting thread
        thread_json = threading.Thread(target=self.output_json, daemon=True)
        thread_log = threading.Thread(target=self.output_log, daemon=True)
        thread_json.start()
        thread_log.start()

        # Keep accepting connections indefinitely
        while True:
            conn, addr = sock.accept()
            client = ClientConnection(conn, self.buffer_size)

            thread_client = threading.Thread(target=self.listen_client,
                                             args=(client,),
                                             daemon=True)
            thread_client.start()

    def listen_client(self, conn: ClientConnection) -> None:
        """Listens to a client for sensor data.

        Args:
            conn: The client connection to receive data from.
        """

        print("  Connected to {}".format(conn.name))
        while True:
            message, success = conn.receive()
            if not success:
                print("  Lost connection with {}".format(conn.name))
                # Remove the entry from the dictionary if necessary
                if conn.name in self._data:
                    with self._lock_data:
                        self._data.pop(conn.name)
                return

            data = pickle.loads(message)

            with self._lock_data:
                self._data[conn.name] = data

    def output_json(self) -> None:
        """Outputs the data to a JSON file to be read by the website."""

        while True:
            with self._lock_data:
                data_out = self._preprocess_data()

            with open(self.file_json, "w") as file:
                json.dump(data_out, file)

            time.sleep(self.interval_json)

    def output_log(self) -> None:
        """Outputs data to a log file.

        Every time the log file is written a line is added according to the
        following format:

        [time]: room_temperature; room_humidity; node data

        Where [node data] contains information for every node in the
        following format:
            node_name: average_cpu_load, memory_use, swap_use, max_temperature;

        If room temperatures are not available, they will be written as N/A.
        """

        while True:
            with self._lock_data:
                line_log = self._create_log_line()

            with open(self.file_log, "a") as file_log:
                file_log.write(line_log)

            time.sleep(self.interval_log)

    def _preprocess_data(self) -> Dict[str, Any]:
        """Preprocesses the data dictionary for output to JSON.

        Returns:
            A dictionary suitable for further processing in the website.
        """

        data_out = dict()

        nodes = []
        for client in self._data:
            if self._data[client]["client_type"] == "node":
                num_cores = len(self._data[client]["cpu_temperature"])

                node_cur = dict()
                node_cur["host_name"] = client
                node_cur["cores"] = num_cores

                processor_cur = dict()
                if num_cores == 0:
                    processor_cur["load_avg"] = "-"
                else:
                    processor_cur["load_avg"] = \
                        sum(self._data[client]["cpu_load"])/num_cores
                processor_cur["temp_max"] = \
                    max(self._data[client]["cpu_temperature"])

                cores_cur = []
                for i in range(num_cores):
                    core_cur = dict()
                    core_cur["number"] = i
                    core_cur["temperature"] = \
                        self._data[client]["cpu_temperature"][i]
                    core_cur["load"] = self._data[client]["cpu_load"][i]
                    cores_cur.append(core_cur)
                processor_cur["cores"] = cores_cur
                node_cur["processor"] = processor_cur

                # Make sure we don't divide by zero if there is no
                # detected memory or swap. This shouldn't happen but it does.
                if self._data[client]["memory_total"] == 0:
                    mem_used = "-"
                else:
                    mem_used = self._data[client]["memory_used"] / \
                               self._data[client]["memory_total"]*100
                if self._data[client]["swap_total"] == 0:
                    swap_used = "-"
                else:
                    swap_used = self._data[client]["swap_used"] / \
                                self._data[client]["swap_total"]*100

                node_cur["memory"] = \
                    {"mem_used": mem_used,
                     "swap_used": swap_used,
                     "mem_total": self._data[client]["memory_total"],
                     "swap_total": self._data[client]["swap_total"]}

                nodes.append(node_cur)
            elif self._data[client]["client_type"] == "front":
                data_out["room_temperature"] = \
                    self._data[client]["room_temperature"]
                data_out["room_humidity"] = \
                    self._data[client]["room_humidity"]

        data_out["nodes"] = nodes

        return data_out

    def _create_log_line(self) -> str:
        """Creates a line for the log file with the current data."""

        # See if there is room temperature data available - otherwise
        # we keep the values of "N/A"
        room_temperature = "N/A"
        room_humidity = "N/A"
        outside_temperature = "N/A"
        outside_humidity = "N/A"
        for client in self._data:
            if self._data[client]["client_type"] == "front":
                room_temperature = \
                    self._data[client]["room_temperature"]
                room_humidity = \
                    self._data[client]["room_humidity"]
                outside_temperature = \
                    self._data[client]["outside_temperature"]
                outside_humidity = \
                    self._data[client]["outside_humidity"]
                break

        line_log = "[{}]: ".format(datetime.datetime.now())
        line_log += "{}; {}; {}; {}; ".format(room_temperature,
                                              room_humidity,
                                              outside_temperature,
                                              outside_humidity)

        # Add client data to the log
        for client in self._data:
            data_cur = self._data[client]
            if data_cur["client_type"] == "node":
                line_log += "{}: {}, {}, {}, {}, {}; " \
                    .format(client,
                            round(len(data_cur["cpu_load"])),
                            round(sum(data_cur["cpu_load"]) /
                                  len(data_cur["cpu_load"]), 2),
                            round(data_cur["memory_used"] /
                                  data_cur["memory_total"] * 100, 2),
                            round(data_cur["swap_used"] /
                                  data_cur["swap_total"] * 100, 2),
                            max(data_cur["cpu_temperature"]))

        # Remove the trailing semicolon and space
        line_log = line_log[:-2]
        line_log += "\n"

        return line_log
